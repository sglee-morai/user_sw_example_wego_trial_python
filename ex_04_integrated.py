from common_import import *

import sys
import time
import math

# 데이터 통신 관련 모듈
from sim_lib.common_type_def import *
from sim_lib.rx_general_data_multi_thread import rx_general_data_multi_thread
from sim_lib.tx_general_data_single_thread import tx_general_data_single_thread
from sim_lib.txrx_general_data_multi_thread import txrx_general_data_multi_thread

# 콘솔 기반 디스플레이 관련 모듈
import sim_lib.my_ui as my_ui

# 차량 위치 / Lane Map 정보 디스플레이 관련 모듈
import sim_lib.read_lanemap as read_lanemap
from multiprocessing import Process


def print_help():
    print('\n----- 다음의 프로그램 사용 방법을 참고하세요 -----')
    print('프로그램의 실행 방법은 다음과 같습니다. (여기서 대괄호는 실제로 입력되는 것은 아닙니다.)')
    print('1) python.exe [스크립트이름] : 스크립트에 설정된 시간(180초)만큼 제어 루프를 동작시킵니다.')
    print('2) python.exe [스크립트이름] [실행시간(초)] : 실행시간(초)만큼 제어 루프를 동작시킵니다.')
    print('')
    print('프로그램은 실행시간이 지나면 자동으로 종료되며, 실행 중 종료를 하려면 콘솔이 활성화된 상태에서 Ctrl+C를 입력하면 됩니다.')


def calculate_control_command(pos_x, pos_y):
    """
    차량의 위치를 받아 간단한 제어 명령을 생성한다.
    50m 구간 전에는 accel 명령을 주고, 50m 구간을 지나면 brake를 잡도록 해두었다.
    :param pos_x: ego vehicle의 x위치
    :param pos_y: ego vehicle의 y위치
    :return: 차량에 전달할 제어 명령
    """
    steering_cmd = 0.0

    if pos_x < 50:
        accel_cmd = 1.0
        brake_cmd = 0.0
    else:
        accel_cmd = 0.0
        brake_cmd = 1.0

    return accel_cmd, brake_cmd, steering_cmd


def main(endtime=180):
    # [사용자 설정값]
    ip = '127.0.0.1'
    obj_gt_data_cmd_port = 7189
    vehicle_tlm_port = 7700
    vehicle_cmd_port = 7800


    # 데이터 수신/송신을 위한 인스턴스 생성
    # - 수신되는 데이터는 별도의 스레드를 통해 데이터를 읽고 버퍼에 넣는다
    #   main 루프에서는 해당 버퍼로부터 데이터를 읽어올 수 있다
    # - 데이터 전송은 main 루프가 실행되는 스레드에서 이루어진다
    # - 데이터 전송과 관련한 인스턴스를 리스트로 모아 정리한다
    data_comm_if_obj_list = list()

    # 주변 물체에 대한 Ground Truth 정보를 받는 인스턴스
    object_gt_tlm_obj = rx_general_data_multi_thread()
    object_gt_tlm_obj.init(ip, obj_gt_data_cmd_port, objects_gt_tlm)
    data_comm_if_obj_list.append(object_gt_tlm_obj)

    # 차량에 명령을 보내고, 차량의 상태 정보를 받는 인스턴스
    vehicle_obj = txrx_general_data_multi_thread()
    vehicle_obj.init(ip, vehicle_tlm_port, ip, vehicle_cmd_port, vehicle_tlm)
    data_comm_if_obj_list.append(vehicle_obj)

    # UI 관련: 콘솔 기반 디스플레이 초기화
    my_ui_obj = my_ui.multiline_cell()
    my_ui_obj.create_ui('Data', 15)

    # UI 관련: matplotlib 기반의 차량 위치 / Map Data 디스플레이 초기화
    manager = read_lanemap.my_multiproc_lifoqueue_manager()
    manager.start()
    lifo = manager.LifoQueue()
    p = Process(target=read_lanemap.lanemap_display_main, args=(lifo, endtime))
    p.daemon = True
    p.start()

    # 데이터 수신을 위한 스레드를 시작
    for obj in data_comm_if_obj_list:
        obj.start_thread_func()

    # 제어 루프 속도
    # [주의 사항] control_freq는 너무 낮거나, 너무 높으면 안 된다.
    # 1) 해당 SW 실행 시 Buffer에 Write를 못한다는 Warning이 지속적으로 발생하면
    #    control_freq가 너무 낮은 것임. 이 때는 control_freq를 올려야 함
    # 2) control_freq는 시뮬레이터의 FPS값보다 낮게 주어야 한다.
    #    이 값을 너무 높이면 시뮬레이터가 전송된 명령을 제대로 처리하지 못할 수 있음
    #    (데모 버전 시뮬레이터 내 버퍼 관련 이슈임)
    control_freq = 25

    # 제어 루프 시작 (Python Console에서 Ctrl+C를 입력하면 종료된다)
    try:
        cnt = 0
        t_offset = time.time()

        # 처음 시간을 한번 측정 (초기화 목적)
        t = time.time() - t_offset
        t_prev = t
        while t < endtime:
            # 현재 시각을 측정하고 종료할 시간이 되면 종료한다
            t = time.time() - t_offset

            # 이전 시간과 비교
            t_elapsed = t - t_prev

            # 제어 루프를 실행할 시점이 되면 루프를 실행한다
            if t_elapsed > 1 / control_freq:
                # 현재 시간을 저장한다
                t = time.time() - t_offset
                t_prev = t

                # 현재 시각 저장 (다음 제어 루프 실행을 위해 필요)
                cnt += 1

                # 디스플레이 시 사용할 로컬 변수
                obstacle_data_updated = False
                egovehicle_data_updated = False
                ego_vehicle_x = 0
                ego_vehicle_y = 0
                ego_vehicle_vel = 0
                ego_vehicle_heading = 0
                obstacle_list_x = []
                obstacle_list_y = []
                lifo_data = dict()

                # 데이터를 수신한다
                # [NOTE] get_buf().read_nonblocking()가 핵심 함수이다.
                # 다른 스레드의 함수를 통해 수신한 데이터는 버퍼에 저장되어 있고 그 버퍼에서 꺼내어 쓰는 것
                obj_data = object_gt_tlm_obj.get_buf().read_nonblocking()
                if obj_data is not None:
                    obstacle_data_updated = True
                    for i in range(len(obj_data.object_list)):
                        obstacle_list_x.append(obj_data.object_list[i].pos_x)
                        obstacle_list_y.append(obj_data.object_list[i].pos_y)

                tlm_data = vehicle_obj.get_buf().read_nonblocking()
                if tlm_data is not None:
                    egovehicle_data_updated = True
                    ego_vehicle_x = tlm_data.pos_x
                    ego_vehicle_y = tlm_data.pos_y
                    ego_vehicle_vel = tlm_data.vel
                    ego_vehicle_heading = tlm_data.yaw

                # 명령값을 만든다. 이때 명령값은 tlm_data 내부의 값을 이용하여 만들면 된다.
                accel_cmd, brake_cmd, steering_cmd = calculate_control_command(ego_vehicle_x, ego_vehicle_y)
                cmd_obj = wego_vehicle_cmd(accel_cmd, brake_cmd, steering_cmd)
                vehicle_obj.send_cmd(cmd_obj)

                # UI 업데이트 (콘솔 UI)
                my_ui_obj.create_new_print_message()

                print_data = '<time>\n[t: {:.3f} cnt: {:d}]\n\n<Data (RX)>\n'.format(t, cnt)
                my_ui_obj.add_to_print_message(print_data)

                my_ui_obj.add_to_print_message('ego vehicle data updated: {}\n'.format(egovehicle_data_updated))
                my_ui_obj.add_to_print_message('ego vehicle pos: ({:.2f}, {:.2f}) (m)\n'.format(
                    ego_vehicle_x, ego_vehicle_y))
                my_ui_obj.add_to_print_message('ego vehicle vel: {:.2f} (km/h), heading: {:.2f} (deg)\n\n'.format(
                    ego_vehicle_vel, ego_vehicle_heading))

                my_ui_obj.add_to_print_message('object data updated: {}\n'.format(obstacle_data_updated))
                my_ui_obj.add_to_print_message('# of objects: {:d}\n'.format(len(obstacle_list_x)))

                my_ui_obj.add_to_print_message('\n<Data (TX)>\n')
                print_data = '{:.2f} {:.2f} {:.2f}\n'.format(accel_cmd, brake_cmd, steering_cmd)
                my_ui_obj.add_to_print_message(print_data)

                my_ui_obj.update_display()

                # UI 업데이트 (Map UI)
                # Ego Vehicle 정보와 Obstacle 정보를 합쳐서 lifo 넣어서
                # 맵 디스플레이를 업데이트하는 프로세스에 전달한다
                lifo_data['ego_vehicle_x'] = ego_vehicle_x
                lifo_data['ego_vehicle_y'] = ego_vehicle_y
                lifo_data['obstacle_list_x'] = obstacle_list_x
                lifo_data['obstacle_list_y'] = obstacle_list_y
                lifo.put(lifo_data)

                # 에러를 검사하고 에러가 있으면 종료한다
                if not obj.is_thread_func_ok():
                    print('[Main  ] [WARN] Error is detected from another thread')
                    break

    # 사용자가 Ctrl+c를 입력하면 while True 구문에서 빠져나온다
    # [NOTE] 일부 실행 환경에서는 Ctrl+c에 반응하지 않을 수 있다
    except KeyboardInterrupt as e:
        print("[Main  ] [INFO] Terminated by KeyboardInterrupt")

    # 각 스레드를 종료시키도록 신호를 보낸다
    print("[Main  ] [INFO] Trying to close threads...")
    for obj in data_comm_if_obj_list:
        obj.signal_thread_func_to_stop()

    # 각 스레드가 정상적으로 종료되도록 기다린다
    for obj in data_comm_if_obj_list:
        obj.get_thread_handle().join(timeout=5.0)
    print("[Main  ] [INFO] Exiting the main program")

    # 각 스레드는 Daemon으로 만들어져 있어서,
    # 위의 함수 join에서 timeout이 발생하더라도 프로그램을 종료하는데는 문제가 없으나
    # 해제되지 않은 리소스가 있을 수 있기 때문에 사용자에게 경고를 띄운다
    for obj in data_comm_if_obj_list:
        if obj.get_thread_handle().is_alive():
            print("[Main  ] [WARN] A child thread is not closing properly.")

    # 프로세스가 정상적으로 종료되도록 기다린다
    print("[Main  ] [INFO] Calling terminate & join functions for matplotlib process")
    p.terminate()
    p.join()

    return


if __name__ == '__main__':
    if len(sys.argv) <= 1:
        main()

    elif len(sys.argv) == 2:
        # 도움말 출력
        if sys.argv[1].lower() == 'help' or sys.argv[1].lower() == '-h':
            print_help()
            sys.exit(0)

        # 인자가 정상 입력되었는지 체크하고 제어 루프 실행
        try:
            end_time = int(sys.argv[1])

        except Exception as e :
            print('[ERROR] 입력된 인자가 잘못되었습니다. 오류 메세지는 다음과 같습니다.')
            print(e)
            print_help()
            sys.exit(-1)

        main(end_time)
    else:
      print('[ERROR] 입력된 인자가 잘못되었습니다.')
      print(help)
      print_help()
