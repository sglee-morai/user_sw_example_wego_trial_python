from common_import import *
import time

# 데이터 통신 관련 모듈
from sim_lib.common_type_def import wego_vehicle_cmd, vehicle_cmd
from sim_lib.tx_general_data_single_thread import tx_general_data_single_thread


def generate_control_command_longitudinal_only(t):
    steering_cmd = 0.0

    if t < 10:
        accel_cmd = 1.0
        brake_cmd = 0.0
    elif t < 20:
        accel_cmd = 0.9
        brake_cmd = 0.0
    elif t < 25:
        accel_cmd = 0.0
        brake_cmd = 0.1
    elif t < 35:
        accel_cmd = 1.0
        brake_cmd = 0.0
    elif t < 40:
        accel_cmd = 0.0
        brake_cmd = 0.1
    else:
        accel_cmd = 0.0
        brake_cmd = 0.0

    return accel_cmd, brake_cmd, steering_cmd


def generate_control_command_circular_motion(t):
    if t < 40:
        accel_cmd = 0.9
        brake_cmd = 0.0
        steering_cmd = -1
    else:
        accel_cmd = 0
        brake_cmd = 0
        steering_cmd = 0

    return accel_cmd, brake_cmd, steering_cmd


def main(endtime=40):
    # [사용자 설정값]
    ip = '127.0.0.1'
    port = 7800
    tx_obj = tx_general_data_single_thread()
    tx_obj.init(ip, port)

    # [사용자 설정값]
    control_freq = 10.0  # Hz

    accel_cmd = 0.0
    brake_cmd = 0.0
    steering_cmd = 0.0

    t_offset = time.time()
    try:
        t_prev = 0.0
        t = 0.0
        while t < endtime:
            # 현재 시간
            t = time.time() - t_offset

            # 이전 시간과 비교
            t_elapsed = t - t_prev

            # 제어 루프를 실행할 시점이 되면 루프를 실행한다
            if t_elapsed > 1/control_freq:

                # 현재 시간을 저장한다
                t = time.time() - t_offset
                t_prev = t

                # 명령값 계산 (둘 중 하나를 사용한다)
                # generate_control_command_longitudinal_only : 직진하기
                # generate_control_command_circular_motion   : 원형으로 돌기

                # 직진 주행 예제
                accel_cmd, brake_cmd, steering_cmd = \
                    generate_control_command_longitudinal_only(t)

                # 원형으로 도는 예제
                # accel_cmd, brake_cmd, steering_cmd = \
                #     generate_control_command_circular_motion(t)


                # 명령값 전송하기
                cmd_obj = wego_vehicle_cmd(accel_cmd, brake_cmd, steering_cmd)
                # cmd_obj = vehicle_cmd(steering_cmd=0, velocity_cmd=20)
                tx_obj.send_cmd(cmd_obj)
                print('t = {:6.2f}  |  cmd = {}'.format(t, cmd_obj.to_string()))

    except KeyboardInterrupt as e:
       print("[Main  ] [INFO] Terminated by KeyboardInterrupt")


if __name__ == '__main__':
    main()
