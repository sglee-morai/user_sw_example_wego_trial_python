from . import win_console_ui as ui

class multiline_cell:
  def __init__(self):
    self.console_size_col, self.consoel_size_row =\
      ui.GetConsoleScreenSize()

  def create_ui(self, cell_title,  cell_row_size):
    # 위 표시줄을 만든다
    print('---------- {} ----------\n'.format(cell_title))

    # 여기서부터 출력을 시작할 것이다
    self.display_pos_col, self.display_pos_row =\
      ui.GetConsoleCursorPosition()

    # 출력할 공간의 크기를 저장한ㄷ
    self.cell_row_size = cell_row_size

    # 출력할 공간을 지나서 아래 표시줄을 만든다
    ui.SetConsoleCursorPosition(\
      self.display_pos_col,
      self.display_pos_row + self.cell_row_size)
    print('\n--------------------------\n')

  def update_display(self, data_to_print=None):
    # 현재 위치로 나중에 커서를 다시 되돌려야 하므로
    # 현재 위치를 기억해둔다
    self.save_current_position()

    ui.SetConsoleCursorPosition(self.display_pos_col, self.display_pos_row)
    if data_to_print == None:
      print(self.make_filled_str(self.message_to_print))
    else:
      print(self.make_filled_str(data_to_print))

    # 기존에 저장했던 위치로 커서를 되돌린다
    self.gobackto_saved_position()

  def save_current_position(self):
    self.backup_pos_col, self.backup_pos_row = ui.GetConsoleCursorPosition()

  def gobackto_saved_position(self):
    ui.SetConsoleCursorPosition(self.backup_pos_col, self.backup_pos_row )

  def create_new_print_message(self):
    self.message_to_print = ''

  def add_to_print_message(self, message):
    self.message_to_print += message

  def make_filled_str(self, str_in, fillchar = ' '):
    assert '\r' not in str_in,\
      'str_in MUST NOT include carriage return.'

    str_list = str_in.split('\n')
    new_str = ''
    for one_line in str_list:
      one_line_filled = one_line.ljust(self.console_size_col, fillchar)

      new_str += one_line_filled

    return new_str
    #return str_in.ljust(self.console_size_col, fillchar)
