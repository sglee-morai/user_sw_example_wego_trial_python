import socket
import select

class rx_general_data_single_thread:
    def __init__(self):
      pass

    def init(self, ip, port, class_type):
      self._ip = ip
      self._port = port
      self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
      self._sock.bind((ip, port))
      self._class_type = class_type

    def receive(self):
      # non blocking call. using timeout = 0.0
      ready_socks, _, _ = select.select([self._sock], [], [], 0.0)
      for socks in ready_socks:
        # recvfrom is a blocking call, but when it is called
        # inside this for loop, it has data to read
        # since select will return sockets that have data to read
        bytes_obj_data, addr = socks.recvfrom(65536)
        # print("size of received message: ", len(data))

        return True, self._class_type.create_object_from_bytes_data(bytes_obj_data)

      return False, None
