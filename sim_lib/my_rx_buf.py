import threading

class my_rx_buf:
  def __init__(self, data_class=None):
    self.lock = threading.Lock()
    self.data = None # in the critical section
    self.data_prev = None # not in the critical section, written only in the read method
    self._data_class = data_class

  def write_blocking_timeout(self, data_in, timeout=0.1):
    # We get the access successfully
    if self._data_class == None:
        return self.write_blocking_timeout_noparse(data_in, timeout)
    else:
        return self.write_blocking_timeout_parse(data_in, timeout)

  def read_nonblocking(self):
    # No timeout
    if (self.lock.acquire(blocking=False)):
      # when cannot getting access to the buffer, use previously saved data
      return self.data_prev

    # We get the access successfully
    data_out = self.data
    self.data_prev = self.data
    self.lock.release()
    return data_out

  def write_blocking_timeout_noparse(self, data_in, timeout=0.1):
    if not self.lock.acquire(blocking=True, timeout = 0.1):
      # Failed to update the buffer
      return False

    # just put bytes data inside the instance property
    self.data = data_in

    self.lock.release()
    return True

  def write_blocking_timeout_parse(self, data_in, timeout=0.1):
    # parse first
    parsed_data = self._data_class.create_object_from_bytes_data(data_in)

    if not self.lock.acquire(blocking=True, timeout = 0.1):
        # Failed to update the buffer
        return False

    # just put bytes data inside the instance property
    self.data = parsed_data

    self.lock.release()
    return True
