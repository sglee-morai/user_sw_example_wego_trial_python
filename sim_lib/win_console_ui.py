import ctypes
from ctypes import c_long, c_wchar_p, c_ulong, c_void_p
from ctypes import byref
from prompt_toolkit.win32_types import CONSOLE_SCREEN_BUFFER_INFO, COORD

# Windows API Document
# https://docs.microsoft.com/en-us/windows/console/getstdhandle
# https://docs.microsoft.com/en-us/windows/console/getconsolescreenbufferinfo
# https://docs.microsoft.com/en-us/windows/console/console-screen-buffer-info-str
# https://docs.microsoft.com/en-us/windows/console/setconsolecursorposition
# https://docs.microsoft.com/en-us/windows/console/writeconsole

gHandle = ctypes.windll.kernel32.GetStdHandle(c_long(-11))

def MoveConsoleCursorPosition(col, row):
  org_col, org_row = GetConsoleCursorPosition()
  SetConsoleCursorPosition(org_col + col, org_row + row)

def SetConsoleCursorPosition(col, row):
  value = col + (row << 16)
  ctypes.windll.kernel32.SetConsoleCursorPosition(gHandle, c_ulong(value))

def addstr(string):
  ctypes.windll.kernel32.WriteConsoleW(gHandle,\
    c_wchar_p(string), c_ulong(len(string)), c_void_p(), None)

def GetConsoleCursorPosition():
  console_buf = CONSOLE_SCREEN_BUFFER_INFO()
  ctypes.windll.kernel32.GetConsoleScreenBufferInfo(gHandle, byref(console_buf))
  col = console_buf.dwCursorPosition.X
  row = console_buf.dwCursorPosition.Y
  return col, row

def GetConsoleScreenSize():
  console_buf = CONSOLE_SCREEN_BUFFER_INFO()
  ctypes.windll.kernel32.GetConsoleScreenBufferInfo(gHandle, byref(console_buf))
  col = console_buf.dwSize.X
  row = console_buf.dwSize.Y
  return col, row

def MakeBlankRow(col, row):
  blank = ''
  for i in range(row):
    blank += ''.ljust(col) + '\n'
  return blank

def ClearRow(row):
  org_col_loc, org_row_loc = GetConsoleCursorPosition()
  #SetConsoleCursorPosition(org_col_loc, org_row_loc - row)

  col_size, _ = GetConsoleScreenSize()
  print(MakeBlankRow(col_size, row), end='')

  SetConsoleCursorPosition(org_col_loc, org_row_loc)

def test_01():
  print('test1\ntest2\ntest3')
  print('12345')

  col, row = GetConsoleCursorPosition()
  print('x,y = ({},{})'.format(col, row))

  # overwrite
  SetConsoleCursorPosition(col, row - 2)
  addstr('_')

  # go back to the original console position
  SetConsoleCursorPosition(col, row)

def test_02():
  print('test1\ntest2\ntest3\ntest4\ntest5\ntest6\ntest7')
  col_org, row_org = GetConsoleCursorPosition()

  MoveConsoleCursorPosition(0, -4)
  ClearRow(2)
  print('TEST4 <-')

  SetConsoleCursorPosition(col_org, row_org)
  print('----------')
  print('finished')
