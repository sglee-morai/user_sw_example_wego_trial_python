import socket

class tx_general_data_single_thread:
  def __init__(self):
    pass

  def init(self, ip, port):
    self._ip = ip
    self._port = port
    self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

  def send_cmd(self, data_obj):
    msg = data_obj.encode_to_bytes()
    self._sock.sendto(msg, (self._ip, self._port))
