import numpy as np
from . import load_csvfile

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import os
import threading
import time

from multiprocessing import Process
from multiprocessing.managers import BaseManager
from queue import LifoQueue


class my_multiproc_lifoqueue_manager(BaseManager):
    pass
my_multiproc_lifoqueue_manager.register('LifoQueue', LifoQueue)


class map_data:
  def __init__(self, filename):
    self.filename = filename

    map = load_csvfile.read_csv_file(filename,
      delimiter = '\t', names = False, skip_header = 0)

    self.east = map[:,0]
    self.north = map[:,1]


class map_drawer:
  def __init__(self, lifo):
    self.fig = plt.figure(figsize=[10, 10])
    self.ax = self.fig.add_subplot(1,1,1)
    self.ax.set_facecolor((0.3, 0.3, 0.3))
    self.lifo = lifo
    #self.ax.set_aspect('equal')
    pass

  def init(self):
    self.read_map_files()
    return self.init_plot()

  def read_map_files(self, dirname=None):

    if dirname is None:
      # default value
      filepath = os.path.realpath(__file__)
      dir = os.path.dirname(filepath)  # 현재 파일의 경로
      dirname = os.path.join(dir, '../lanemap')
      dirname = os.path.normpath(dirname)
    self.dirname = dirname

    # 파일 이름 리스트
    self.center_path_filename = get_filename_list(dirname,
      lambda x: ('Spline_TrialPath' in x) and x != 'Spline_TrialPath.txt')

    self.normal_lane_filenames = get_filename_list(dirname,
      lambda x: 'Spline_LightPosition' in x)

    self.center_line_filenames = get_filename_list(dirname,
       lambda x: x == 'Spline_TrialPath.txt')

    # 맵 정보를 담고 있는 객체의 리스트
    self.center_path_lanemaps = list()
    for filename in self.center_path_filename:
      self.center_path_lanemaps.append(map_data(filename))

    self.normal_lanes = list()
    for filename in self.normal_lane_filenames:
      self.normal_lanes.append(map_data(filename))

    self.center_lines = list()
    for filename in self.center_line_filenames:
      self.center_lines.append(map_data(filename))

  def init_plot(self):
    #plt.title('Lane Map')

    self.draw_background()

    # 계속 업데이트할 객체
    self.ego_vehicle_line_obj, = self.ax.plot([], [], 'cH', markersize=15, label='ERP-42')
    self.ego_vehicle_line_obj.set_data([], [])

    self.obstacle_line_obj, = self.ax.plot([], [], 'rX', markersize=10, label='Obstacle')
    self.obstacle_line_obj.set_data([], [])

    self.ax.legend(loc='upper left')

    return self.ego_vehicle_line_obj,

  def draw_background(self):
    for lanes in self.center_path_lanemaps:
      plt.plot(lanes.east, lanes.north, 'b:', linewidth=3, label='Vehicle Path')

    for lanes in self.normal_lanes:
      plt.plot(lanes.east, lanes.north, 'w--', linewidth=5, label='Normal Lane')

    for lanes in self.center_lines:
      plt.plot(lanes.east, lanes.north, 'y-', linewidth=8, label='Centerline')

    distance_sign_x = np.arange(0, 300, 5)
    sign_y = -9
    for sign_x in distance_sign_x:
      txt = plt.text(sign_x, sign_y, '{:d} m'.format(sign_x), color='r')
      txt.set_rotation(270)


  def draw_point(self, data):
    # print('drawpoint called')
    if not self.lifo.empty():
      lifo_data = self.lifo.get_nowait()

      if ('ego_vehicle_x' in lifo_data) and ('ego_vehicle_y' in lifo_data):
        ego_x = lifo_data['ego_vehicle_x']
        ego_y = lifo_data['ego_vehicle_y']
        self.ego_vehicle_line_obj.set_data(ego_x, ego_y)

        # ego vehicle 주변으로 20 x 20 공간을 보여준다면,
        # min 또는 max 값이 맵의 최대값 또는 최소값 보다 작아지지는 않는지 확인한다
        display_range = 10
        xlim_min = ego_x - display_range
        xlim_max = ego_x + display_range
        ylim_min = ego_y - display_range
        ylim_max = ego_y + display_range

        map_x_min = 0
        map_x_max = 300
        map_y_min = -10
        map_y_max = 10
        if xlim_min < map_x_min:
          xlim_min = map_x_min
          xlim_max = map_x_min + 2 * display_range
        if xlim_max > map_x_max:
          xlim_max = map_x_max
          xlim_min = map_x_max - 2 * display_range
        if ylim_min < map_y_min:
          ylim_min = map_y_min
          ylim_max = map_y_min + 2 * display_range
        if ylim_max > map_y_max:
          ylim_max = map_y_max
          ylim_min = map_y_max - 2 * display_range

        self.ax.set_xlim([xlim_min, xlim_max])
        self.ax.set_ylim([ylim_min, ylim_max])

      if ('obstacle_list_x' in lifo_data) and ('obstacle_list_y' in lifo_data):
        obs_x = lifo_data['obstacle_list_x']
        obs_y = lifo_data['obstacle_list_y']
        self.obstacle_line_obj.set_data(obs_x, obs_y)

      while not self.lifo.empty():
        self.lifo.get_nowait()
    # print('pos_x = {:.2f}, pos_y = {:.2f}'.format(self.point_pos_x_prev, self.point_pos_y_prev))

    return self.ego_vehicle_line_obj,


def get_filename_list(dirname, check_func):
  filename_list = list()

  filenames = os.listdir(dirname)
  for filename in filenames:
    if check_func(filename):
      filename_list.append(os.path.join(dirname, filename))

  return filename_list


def map_gui_main():
  dirname = 'lanemap'

  # 파일 이름 리스트
  center_path_filename = get_filename_list(dirname,
    lambda x: 'Highway_Center_Path' in x)

  line_filename = get_filename_list(dirname,
    lambda x: 'Highway_Line' in x)

  # 맵 정보를 담고 있는 객체의 리스트
  center_path_lanemaps = list()
  for filename in center_path_filename:
    center_path_lanemaps.append(map_data(filename))

  linemaps = list()
  for filename in line_filename:
    linemaps.append(map_data(filename))

  # 그려보기
  fig = plt.figure()
  plt.title('Lane Map')

  ax = fig.add_subplot(1,1,1)
  ax.set_aspect('equal')

  for lanes in center_path_lanemaps:
    plt.plot(lanes.east, lanes.north, 'b-.')

  for lanes in linemaps:
    plt.plot(lanes.east, lanes.north, 'r-.')


def lanemap_display_main(lifo, endtime):
  obj = map_drawer(lifo)

  ani = animation.FuncAnimation(obj.fig, obj.draw_point,
                                blit=False, interval=40,
                                repeat=False, init_func=obj.init)

  plt.show()

def program_main():
  manager = my_multiproc_lifoqueue_manager()
  manager.start()
  lifo = manager.LifoQueue()

  p = Process(target=lanemap_display_main, args=(lifo, endtime))
  print('[INFO] Display Process Starts')
  p.start()

  print('[INFO] Mainloop Starts')
  t_offset = time.time()
  t = time.time() - t_offset
  while t < 12:
    if not lifo.full():
      pos_x = 10.0 * t - 250
      pos_y = 17.5 * t + 1075
      lifo.put_nowait([pos_x, pos_y])

    t = time.time() - t_offset
    pass

if __name__ == '__main__':
  # 그냥 맵 한번 보고 싶으면
  #map_gui_main()
  #plt.show()

  # 제대로 돌려보려면
  program_main()

# ----------------
