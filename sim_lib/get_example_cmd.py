from .common_type_def import *

def get_example_cmd_steering(t):
  if t < 5 :
    steering_cmd = 0.5
  elif t < 10:
    steering_cmd = -0.5
  elif t < 15:
    steering_cmd = 1
  elif t < 20:
    steering_cmd = -1
  elif t < 25:
    steering_cmd = 0.5
  elif t < 30:
    steering_cmd = -0.5
  else:
    steering_cmd = 0

  velocity_cmd = 0
  return vehicle_cmd(steering_cmd, velocity_cmd)

def get_example_cmd_velocity(t):
  steering_cmd = 0

  if t < 10:
    velocity_cmd = 30
  elif t < 20:
    velocity_cmd = 10
  elif t < 30:
    velocity_cmd = 50
  elif t < 40:
    velocity_cmd = 20
  elif t < 50:
    velocity_cmd = 70
  elif t < 60:
    velocity_cmd = 20
  elif t < 70:
    velocity_cmd = 50
  elif t < 80:
    velocity_cmd = 10
  elif t < 90:
    velocity_cmd = 30
  else:
    velocity_cmd = 0

  return vehicle_cmd(steering_cmd, velocity_cmd)
