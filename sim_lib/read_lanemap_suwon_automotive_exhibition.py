import numpy as np
from . import load_csvfile

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import os
import threading
import time

from multiprocessing import Process
from multiprocessing.managers import BaseManager
from queue import LifoQueue


class my_multiproc_lifoqueue_manager(BaseManager):
    pass
my_multiproc_lifoqueue_manager.register('LifoQueue', LifoQueue)


class map_data:
  def __init__(self, filename):
    self.filename = filename

    map = load_csvfile.read_csv_file(filename,
      delimiter = '\t', names = False, skip_header = 0)

    self.east = map[:,0]
    self.north = map[:,1]


class map_drawer:
  def __init__(self, lifo):
    self.point_pos_x_prev = 0
    self.point_pos_y_prev = 0

    self.fig = plt.figure(figsize=[10, 10])
    self.ax = self.fig.add_subplot(1,1,1)
    self.ax.set_facecolor((0.3, 0.3, 0.3))
    self.lifo = lifo
    #self.ax.set_aspect('equal')
    pass

  def init(self):
    self.read_map_files()
    return self.init_plot()

  def read_map_files(self, dirname='lanemap'):
    self.dirname = dirname

    # 파일 이름 리스트
    self.center_path_filename = get_filename_list(dirname,
      lambda x: 'MORAI_K_CityDemoPath' in x)

    # self.line_filename = get_filename_list(dirname,
    #   lambda x: 'Highway_Line' in x)

    # 맵 정보를 담고 있는 객체의 리스트
    self.center_path_lanemaps = list()
    for filename in self.center_path_filename:
      self.center_path_lanemaps.append(map_data(filename))

    # self.linemaps = list()
    # for filename in self.line_filename:
    #   self.linemaps.append(map_data(filename))

  def init_plot(self):
    plt.title('Lane Map')

    self.draw_background()

    # 계속 업데이트할 객체
    self.line, = self.ax.plot([], [], 'cX', markersize = 15)
    self.line.set_data([], [])

    return self.line,

  def draw_background(self):
    for lanes in self.center_path_lanemaps:
      plt.plot(lanes.east, lanes.north, 'b-.')

    # for lanes in self.linemaps:
    #   plt.plot(lanes.east, lanes.north, 'w-.')

  def draw_point(self, data):
    # print('drawpoint called')
    if self.lifo.empty():
      pos_x = self.point_pos_x_prev
      pos_y = self.point_pos_y_prev
    else:
      pos_x, pos_y = self.lifo.get_nowait()
      self.point_pos_x_prev = pos_x
      self.point_pos_y_prev = pos_y
      while not self.lifo.empty():
        self.lifo.get_nowait()
    # print('pos_x = {:.2f}, pos_y = {:.2f}'.format(self.point_pos_x_prev, self.point_pos_y_prev))
    self.line.set_data(pos_x, pos_y)
    return self.line,


def get_filename_list(dirname, check_func):
  filename_list = list()

  filenames = os.listdir(dirname)
  for filename in filenames:
    if check_func(filename):
      filename_list.append(os.path.join(dirname, filename))

  return filename_list


def map_gui_main(dirname = 'lanemap'):


  # 파일 이름 리스트
  center_path_filename = get_filename_list(dirname,
    lambda x: 'MORAI_K_CityDemoPath' in x)

  # line_filename = get_filename_list(dirname,
  #   lambda x: 'Highway_Line' in x)

  # 맵 정보를 담고 있는 객체의 리스트
  center_path_lanemaps = list()
  for filename in center_path_filename:
    center_path_lanemaps.append(map_data(filename))

  # linemaps = list()
  # for filename in line_filename:
  #   linemaps.append(map_data(filename))

  # 그려보기
  fig = plt.figure()
  plt.title('Lane Map')

  ax = fig.add_subplot(1,1,1)
  ax.set_aspect('equal')

  for lanes in center_path_lanemaps:
    plt.plot(lanes.east, lanes.north, 'b-.')

  # for lanes in linemaps:
  #   plt.plot(lanes.east, lanes.north, 'r-.')

def lanemap_display_main(lifo, endtime):
  obj = map_drawer(lifo)

  ani = animation.FuncAnimation(obj.fig, obj.draw_point,
                                blit=False, interval=40,
                                repeat=False, init_func=obj.init)

  plt.show()


if __name__ == '__main__':
  # 그냥 맵 한번 보고 싶으면
  # map_gui_main(dirname='../lanemap')
  # plt.show()

  # 제대로 돌려보려면
  program_main()

# ----------------
