import my_ui as ui
import time
import random
from . import win_console_ui as win_ui

def make_filled_str_test():
  ui_obj = ui.multiline_cell()

  print(ui_obj.make_filled_str('test1', '_'))
  print(ui_obj.make_filled_str('test2', '_'))
  print(ui_obj.make_filled_str('test3', '_'))

  data_in = 'test1\ntest2\ntest3'
  data_out = ui_obj.make_filled_str(data_in, '_')
  print(data_out)

  data_in = 'TEST1\nTEST2'
  ui_obj.create_new_print_message()


def func_test():
  ui_obj = ui.multiline_cell()
  ui_obj.create_ui('Test Data', 2)

  t_offset = time.time()
  t = time.time() - t_offset
  while t < 3:
    t = time.time() - t_offset
    a = random.random()



    print_data = '{}'.format(a)
    ui_obj.update_display(print_data)

    time.sleep(0.1)

def test1():
  ui_obj = ui.multiline_cell()
  ui_obj.create_new_print_message()
  ui_obj.add_to_print_message('test1\n')
  # ui_obj.add_to_print_message('test2\n')
  # ui_obj.add_to_print_message('test3\n')

  # ui_obj.create_ui('Test Data', 5)
  # ui_obj.update_display()

def test2():
  ui_obj = ui.multiline_cell()
  ui_obj.create_new_print_message()
  ui_obj.add_to_print_message('test1\n')
  ui_obj.add_to_print_message('test2\n')
  ui_obj.add_to_print_message('test3\n')

  ui_obj.create_ui('Test Data', 5)
  ui_obj.update_display()

def test3():
  ui_obj = ui.multiline_cell()
  ui_obj.create_new_print_message()

test2()
