import threading
import socket
import select
from .my_rx_buf import my_rx_buf
from .common_type_def import *

# TODO(sglee): 디버깅 용도로 남겨둔 플래그.
# 이를 ON하고
# - _save_parsed_data를 ON하면 반응성이 매우 좋지 않게 된다
# - 그런데, 이를 OFF하면 반응성이 괜찮다
# 왜 그렇지?
TODO_DEBUG_PRINT_INSIDE = False


class rx_general_data_multi_thread:
  def __init__(self, save_parsed_data=True):
    self._thread_func_stop_flag = False
    self._thread_func_error_flag = False
    self._thread_handle = None
    self._save_parsed_data = save_parsed_data

  def init(self, ip, port, data_class):
    # print('[Thread] [TRACE] vehicle_tlm_rx.init_func_rx_vehicle_tlm')
    self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
    self._sock.bind((ip, port))
    self._data_class = data_class

    if self._save_parsed_data:
      # data received by UDP will be parsed, and then saved in the buffer
      # parse func will be called when reading the data
      self._buf = my_rx_buf(data_class)
    else:
      # data received by UDP will be saved directly
      self._buf = my_rx_buf()

  def close(self):
    # Normally, _sock will be closed automatically when the dtor of this class is called
    # Thus, it is not necessary to call this unless you do want to close the socket at a certain time
    self._sock.close()

  def thread_func(self):
    # print('[Thread] [TRACE] vehicle_tlm_rx.thread_func_rx_vehicle_tlm before while')
    try:
      while True:
        if self.is_thread_func_signaled_to_stop():
          # print('[Thread] [TRACE] vehicle_tlm_rx.thread_func_rx_vehicle_tlm is closing due to external signal')
          break

        # non blocking call. using timeout = 0.0
        ready_socks, _, _ = select.select([self._sock], [], [], 0.0)
        for socks in ready_socks:
          # recvfrom is a blocking call, but when it is called
          # inside this for loop, it has data to read
          # since select will return sockets that have data to read
          bytes_obj_data, addr = socks.recvfrom(65536)
          # print("size of received message: ", len(data))

          result = self._buf.write_blocking_timeout(bytes_obj_data)
          if not result:
            print('[Thread] [WARN] failed to write (Thread: {})'.format(threading.currentThread().getName()))
            continue

          if TODO_DEBUG_PRINT_INSIDE:
            if self._save_parsed_data:
              data_parsed = self._buf.read_nonblocking()
            else:
              data_parsed = self._data_class.create_object_from_bytes_data(bytes_obj_data)

            if data_parsed == None:
                print('[ERROR] data_pared is None!')
            else:
                print('[Thread] [TRACE] data_parsed: {}'.format(data_parsed.to_string()))

          # point_list = decode_velodyne16(data)
          # print('len_point: {}'.format(len(point_list)))
          # print('len_point: {}\ndata :{}'.format(len(point_list), point_list))

    except Exception as e:
      self._thread_func_error_flag = True
      print('[Thread] [ERROR] Error inside while True loop (Thread: {})'.format(threading.currentThread().getName()))
      print('[Thread] [ERROR] Exception Message: ', e)
      # print('[Thread] [ERROR] Exception Message: {}'.format(e.message))

    print('[Thread] [INFO] Exiting the {} thread'.format(threading.currentThread().getName()))
    return

  def start_thread_func(self, thread_name=None):
    if thread_name == None:
      thread_name = self._data_class.__name__
      
    self._thread_handle = threading.Thread(target=self.thread_func, name=thread_name)
    self._thread_handle.daemon = True
    self._thread_handle.start()
    return self._thread_handle

  def get_buf(self):
    return self._buf

  def get_thread_handle(self):
    return self._thread_handle

  def signal_thread_func_to_stop(self):
    self._thread_func_stop_flag = True

  def is_thread_func_ok(self):
    return self._thread_func_error_flag != True

  def is_thread_func_signaled_to_stop(self):
    return self._thread_func_stop_flag == True
