import struct

class point:
  def __init__(self, x=0, y=0):
    self.x = x
    self.y = y

  def to_string(self):
    return '({:.6f}, {:.6f})'.format(self.x, self.y)
#####################################################################################################################

class velodyne16_gt_data:
  def __init__(self, data=None):
    if data == None:
      self.number_of_points = 0
      self.point_list = list()
    else:
      self.decode_from_bytes(data)

  def to_string(self):
    if self.number_of_points <= 3:
      return '[num_points = {:d}]'.format(self.number_of_points)
    else:
      return '[num_points = {:d}, point = [{}, {}, ..., {}]]'.format(\
        self.number_of_points,
        self.point_list[0].to_string(),
        self.point_list[1].to_string(),
        self.point_list[self.number_of_points - 1].to_string())

  def decode_from_bytes(self, data):
    # data comming in bytes object
    # note that data is stored in big-endian format
    len_header = 4
    len_point = 8

    assert len(data) >= len_header, \
        '[ERROR] cannot parse this data. Data length must be greater than 4. Data passed = {}'.format(data)
    num_points = (struct.unpack('<I', data[0:4]))[0]

    len_total_data_required = len_header + num_points * len_point

    assert len(data) >= (len_total_data_required), \
        '[ERROR] cannot parse this data. Data length must be greater than {} (num_points = {}). Data passed = {}'.format(len_total_data_required, num_points, data)

    self.point_list = list()
    ptr = 4
    for i in range(num_points):
      # Little-endian
      xy = struct.unpack('<ff', data[ptr : ptr+len_point])
      self.point_list.append(point(xy[0], xy[1]))
      ptr += len_point

    self.number_of_points = len(self.point_list)
    return

  @staticmethod
  def create_object_from_bytes_data(data):
    return velodyne16_gt_data(data)
#####################################################################################################################

class camera_for_lane_detection_gt_data:
  def __init__(self, data=None):
    if data == None:
      self.detection_condition = 0
      self.point_list = list()
    else:
      pass

  def to_string(self):
    if self.detection_condition:
      return '[detected = {}, point = [{}, {}, {}, {}]]'.format(\
        self.detection_condition,
        self.point_list[0].to_string(),
        self.point_list[1].to_string(),
        self.point_list[2].to_string(),
        self.point_list[3].to_string())
    else:
      return '[detected = {}]'.format(self.detection_condition)

  def decode_from_bytes(self, data):
    len_total_data_required = 35
    assert len(data) == len_total_data_required, \
      '[ERROR] cannot parse this data. len(data) must be {}. data = {}'.format(\
        len_total_data_required, data)
    data_unpacked = struct.unpack('<?ffffffff', data)
    self.detection_condition = data_unpacked[0]
    for i in range(4):
      self.point_list.append(\
        point(data_unpacked[i*2 + 1], data_unpacked[i*2 + 2]))

  @staticmethod
  def create_object_from_bytes_data(data):
    return camera_for_lane_detection_gt_data(data)
#####################################################################################################################

class camera_for_crosswalk:
  def __init__(self, data=None):
    if data == None:
        self.detection_condition = False
        self.distance_to_crosswalk = 0
    else:
        self.decode_from_bytes(data)

  def to_string(self):
    if self.detection_condition:
      return '[detected = {}, distance = {:.4f}]'.format(\
        self.detection_condition, self.distance_to_crosswalk)
    else:
      return '[detected = {}]'.format(self.detection_condition)

  def decode_from_bytes(self, data):
    len_total_data_required = 5
    assert len(data) == len_total_data_required, \
      '[ERROR] cannot parse this data. len(data) must be {}. data = {}'.format(\
        len_total_data_required, data)

    data_unpacked = struct.unpack('<?f', data)
    self.detection_condition = data_unpacked[0]
    self.distance_to_crosswalk = data_unpacked[1]

  @staticmethod
  def create_object_from_bytes_data(data):
    return camera_for_crosswalk(data)
#####################################################################################################################

class parking:
  def __init__(self, data=None):
    if data == None:
      self.point = point(0, 0)
      self.angle_in_rad = 0
    else:
      self.decode_from_bytes(data)

  def to_string(self):
    return '[x = {:.6f}, y = {:.6f}, angle = {:.6f} rad]'.format(\
      self.point.x, self.point.y, self.angle_in_rad)

  def decode_from_bytes(self, data):
    len_total_data_required = 12
    assert len(data) == len_total_data_required, \
      '[ERROR] cannot parse this data. len(data) must be {}. data = {}'.format(\
        len_total_data_required, data)

    data_unpacked = struct.unpack('<fff', data)
    self.point = point(data_unpacked[0], data_unpacked[1])
    self.angle_in_rad = data_unpacked[2]

  @staticmethod
  def create_object_from_bytes_data(data):
    return parking(data)
#####################################################################################################################

class traffic_light:
  def __init__(self, data=None):
    if data == None:
      self.detection_condition = False
      self.detection_signal = 0 # 0:red, 1:green, 2:left turn
    else:
      self.decode_from_bytes(data)

  def to_string(self):
    if self.detection_condition:
      return '[detected = {}, detection_signal = {}]'.format(\
        self.detection_condition, self.detection_signal)
    else:
      return '[detected = {}]'.format(self.detection_condition)

  def decode_from_bytes(self, data):
    len_total_data_required = 2
    assert len(data) >= len_total_data_required, \
      '[ERROR] cannot parse this data. len(data) must be {}. data = {}'.format(\
        len_total_data_required, data)

    data_unpacked = struct.unpack('<?B', data[0:2])
    self.detection_condition = data_unpacked[0]
    self.detection_signal = data_unpacked[1]

  @staticmethod
  def create_object_from_bytes_data(data):
    return traffic_light(data)
#####################################################################################################################

class vehicle_tlm:
  def __init__(self, data=None):
    if data == None:
      # ENU coordinate
      self.pos_x = 0
      self.pos_y = 0
      self.pos_z = 0
      self.roll = 0
      self.pitch = 0
      self.yaw = 0
      self.vel = 0
      self.steer = 0
      self.trust_data = False

    else:
      self.decode_from_bytes(data)

  def to_string(self):
    if self.trust_data:
      return '[pos = ({:.2f}, {:.2f}, {:.2f}) attitude = ({:.2f}, {:.2f}, {:.2f}), vel = {:.2f}, steer = {:.2f}]'.format(\
        self.pos_x, self.pos_y, self.pos_z, self.roll, self.pitch, self.yaw, self.vel, self.steer, self.trust_data)
    else:
      return ''

  def decode_from_bytes(self, data):
    len_total_data_required = 37
    assert len(data) >= len_total_data_required, \
      '[ERROR] cannot parse this data. len(data) must be {}. data = {}'.format(\
        len_total_data_required, data)

    data_unpacked = struct.unpack('<2sffffffff3s', data)
    morai_actl = (data_unpacked[0] + data_unpacked[9]).decode('utf-8')
    if morai_actl != 'MORAI':
      self.pos_x = 0
      self.pos_y = 0
      self.pos_z = 0
      self.roll = 0
      self.pitch = 0
      self.yaw = 0
      self.vel = 0
      self.steer = 0
      self.trust_data = False
    else:
      self.pos_x = data_unpacked[1]
      self.pos_y = data_unpacked[2]
      self.pos_z = data_unpacked[3]
      self.roll = data_unpacked[4]
      self.pitch = data_unpacked[5]
      self.yaw = data_unpacked[6]
      self.vel = data_unpacked[7]
      self.steer = data_unpacked[8]
      self.trust_data = True

  @staticmethod
  def create_object_from_bytes_data(data):
    return vehicle_tlm(data)
#####################################################################################################################

class vehicle_cmd: #for SDS
  def __init__(self, steering_cmd, velocity_cmd):
    self.steering_cmd = steering_cmd
    self.velocity_cmd = velocity_cmd

  def to_string(self):
    return '[steering_cmd = {:.2f}, velocity_cmd = {:.2f}]'.format(\
      self.steering_cmd, self.velocity_cmd)

  def encode_to_bytes(self):
    # Little-endian
    fmt = '<ff'
    bytes_obj = struct.pack(fmt, self.steering_cmd, self.velocity_cmd)
    return bytes_obj
#####################################################################################################################

class wego_vehicle_cmd:
  def __init__(self, accel_cmd, brake_cmd, steering_cmd):

    accel_cmd = abs(accel_cmd)
    if accel_cmd > 1:
      accel_cmd = 1

    brake_cmd = abs(brake_cmd)
    if brake_cmd > 1:
      brake_cmd = 1

    if steering_cmd < -1:
      steering_cmd = -1
    elif steering_cmd > 1:
      steering_cmd = 1

    self.accel_cmd = accel_cmd
    self.brake_cmd = brake_cmd
    self.steering_cmd = steering_cmd

  def to_string(self):
    return '[accel_cmd = {:.2f}, brake_cmd = {:.2f}, steering_cmd = {:.2f}]'.format(\
      self.accel_cmd, self.brake_cmd, self.steering_cmd)

  def encode_to_bytes(self):
    # Little-endian
    fmt = '<ccfffccc'
    bytes_obj = struct.pack(fmt, b'M', b'O', self.accel_cmd, self.brake_cmd, self.steering_cmd, b'R', b'A', b'I')
    # print(bytes_obj)
    return bytes_obj
#####################################################################################################################

class object_info:
  def __init__(self, obj_type=0, pos_x=0, pos_y=0, heading=0, size_x=0, size_y=0):
    # ENU coordinate
    self.obj_type = obj_type
    self.pos_x = pos_x
    self.pos_y = pos_y
    self.heading = heading
    self.size_x = size_x
    self.size_y = size_y

  def to_string(self):
    return '[pos = object Type = {:d}, position = ({:.2f}, {:.2f}) size = ({:.2f}, {:.2f}, {:.2f})]'.format(\
      self.obj_type, self.pos_x, self.pos_y, self.heading, self.size_x, self.size_y)

#####################################################################################################################

class objects_gt_tlm:
  def __init__(self, data=None):
    if data == None:
      self.number_of_objects = 0
      self.object_list = list()
    else:
      self.decode_from_bytes(data)

  def to_string(self):
    if self.number_of_objects <= 0:
      return '[num_points = {:d}]'.format(
        self.number_of_objects)
    elif self.number_of_objects == 1:
      return '[num_points = {:d}, point = [{}, ...]]'.format(
        self.number_of_objects,
        self.object_list[0].to_string())
    elif self.number_of_objects == 2:
      return '[num_points = {:d}, point = [{}, {}]]'.format(
        self.number_of_objects,
        self.object_list[0].to_string(),
        self.object_list[1].to_string())
    elif self.number_of_objects == 3:
      return '[num_points = {:d}, point = [{}, {}, {}]]'.format(
        self.number_of_objects,
        self.object_list[0].to_string(),
        self.object_list[1].to_string(),
        self.object_list[2].to_string())
    else:
      return '[num_points = {:d}, point = [{}, {}, ..., {}]]'.format(
        self.number_of_objects,
        self.object_list[0].to_string(),
        self.object_list[1].to_string(),
        self.object_list[self.number_of_objects - 1].to_string())

  def decode_from_bytes(self, data):
    # data comming in bytes object
    # note that data is stored in big-endian format
    len_header = 4
    len_obj_info = 21

    assert len(data) >= len_header, \
      '[ERROR] cannot parse this data. Data length must be greater than 4. Data passed = {}'.format(data)
    num_points = (struct.unpack('<I', data[0:4]))[0]

    len_total_data_required = len_header + num_points * len_obj_info

    assert len(data) >= (len_total_data_required), \
      '[ERROR] cannot parse this data. Data length must be greater than {} (num_points = {}). Data passed = {}'.format(
        len_total_data_required, num_points, data)

    self.object_list = list()
    ptr = 4
    for i in range(num_points):
      # Little-endian
      obj_tlm = struct.unpack('<Bfffff', data[ptr: ptr + len_obj_info])
      self.object_list.append(object_info(obj_tlm[0], obj_tlm[1], obj_tlm[2], obj_tlm[3], obj_tlm[4], obj_tlm[5]))
      ptr += len_obj_info

    self.number_of_objects = len(self.object_list)
    return

  @staticmethod
  def create_object_from_bytes_data(data):
    return objects_gt_tlm(data)
