from common_import import *
import time

# 데이터 통신 관련 모듈
from sim_lib.common_type_def import objects_gt_tlm
from sim_lib.rx_general_data_single_thread import rx_general_data_single_thread


"""
[중요] 이 예제에서는 control_freq 값에 따라
수신하는 값에 딜레이가 발생할 수 있다.

control_freq가 시뮬레이터의 fps 보다 작은 값으로 설정되어 있으면
시뮬레이터에서 보낸 데이터 중 일부가 처리되지 않고 버퍼에 쌓여
시뮬레이터와 본 코드의 출력 사이에 딜레이가 발생하게 된다.

본 예제는 데이터를 받는 최소한의 코드만을 담고 있어 이러한 문제가 발생하는 것이며,
심화 예제(ex_04_integrated)에서는 이러한 문제가 해결하지 않는다.
"""


def main(endtime=40):
    # [사용자 설정값]
    ip = '127.0.0.1'
    port = 7189
    rx_obj = rx_general_data_single_thread()
    rx_obj.init(ip, port, objects_gt_tlm)

    # [사용자 설정값]
    control_freq = 150.0  # Hz

    t_offset = time.time()
    try:
        t_prev = 0.0
        t = 0.0
        while t < endtime:
            # 현재 시간
            t = time.time() - t_offset

            # 이전 시간과 비교
            t_elapsed = t - t_prev

            # 제어 루프를 실행할 시점이 되면 루프를 실행한다
            # [중요] 시뮬레이터에서 보내는 값이 빨리 업데이트되지 않고 딜레이가 발생하면
            # 이 코드에서는 control_freq 값을 키워야 한다
            if t_elapsed > 1/control_freq:

                # 현재 시간을 저장한다
                t = time.time() - t_offset
                t_prev = t

                rcv_result, data = rx_obj.receive()
                if rcv_result:
                    print('t = {:6.4f}  |  msg = {}'.format(t, data.to_string()))

    except KeyboardInterrupt as e:
       print("[Main  ] [INFO] Terminated by KeyboardInterrupt")


if __name__ == '__main__':
    main()